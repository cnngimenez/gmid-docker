# Build gmid for alpine distro.
FROM alpine as builder
WORKDIR /build

RUN apk update && apk upgrade && apk add --repository=https://dl-cdn.alpinelinux.org/alpine/edge/main alpine-sdk linux-headers bison libretls-dev libretls libevent-dev libevent

COPY . .

RUN ls -l; cd gmid; ./configure; make clean; make

# Create the image.
FROM alpine

RUN apk update && apk upgrade && apk add --repository=https://dl-cdn.alpinelinux.org/alpine/edge/main bison libretls libevent

COPY --from=builder /build/gmid/gmid /bin/gmid
COPY --from=builder /build/gmid/site /opt/gmid/site
COPY --from=builder /build/run-gmid.sh /bin/run-gmid.sh

ENV GMID_CONFIGFILE="" GMID_PARAMS="-fv"

ENTRYPOINT /bin/sh /bin/run-gmid.sh

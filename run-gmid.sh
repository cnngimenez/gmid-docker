#! /bin/sh

cd /opt/gmid # Just in case change the working directory.

echo "Running gmid with $GMID_PARAMS"

if test -z "$GMID_CONFIGFILE"; then
    /bin/gmid $GMID_PARAMS /opt/gmid/site
else
    echo "Using config file: $GMID_CONFIGFILE"
    /bin/gmid $GMID_PARAMS -c "$GMID_CONFIGFILE"   
fi
